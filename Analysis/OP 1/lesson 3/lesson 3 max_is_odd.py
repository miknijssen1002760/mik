y = int(input())
x = int(input())
z = int(input())

max = 0
if x%2!=0:
    max = x
elif y%2!=0:
    max = y
elif z%2!=0:
    max = z
else:
    max = 'there are no odd numbers'
if y>max:
    if y%2!=0:
        max = y
if z>max:
    if z%2!=0:
        max = z

print(max)