# 1002760 Mik Nijssen inf1K
N = input()
d = len(N)
N = int(N)
CheckDigit=N%10
DigitSum = 0
while d>1:
    N = N//10
    DigitSum += N%10
    d=d-1

if DigitSum%10 == CheckDigit:
    print('VALID')
else:
    print('INVALID')
