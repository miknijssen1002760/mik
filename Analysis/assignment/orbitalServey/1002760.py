#1002760 Mik Nijssen INF1k
def DecimalToBinary(num):
    binary = ''
    while num>=1:
        binary += str(num%2)
        num = num//2
    return binary

width = int(input())
data = int(input())

max = 0
sequence = DecimalToBinary(data)
sequence += '0'*(width*(len(sequence)//width)-len(sequence))
h = len(sequence)//width
for j in range(width):
    pilar = 0 
    for i in range(len(sequence)//width):
        if sequence[i*width + j]=='1':
            pilar+=1
            if pilar>max:
                max=pilar
        else:
            break
print(max)