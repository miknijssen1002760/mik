gradeMath = 6.4
gradeEnglish = 7.5
gradeFrench = 4.2

# Did you pass Math?
answer1 = str(gradeMath >= 5.5) + ', I got a ' \
+ str(gradeMath) + ' for Math'

# Did you pass English?
answer2 = str(gradeEnglish >= 5.5) + ', I got a ' \
+ str(gradeEnglish) + ' for English'

# Did you pass French?
answer3 = str(gradeFrench >= 5.5) + ', I got a ' \
+ str(gradeFrench) + ' for French'
print()