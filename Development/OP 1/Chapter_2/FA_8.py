s1 = 'celebrate success'
s2 = 'never regard study as a duty'
s3 = 'but'
s4 = 'enviable opportunity'
s5 = 'learn the lessons'
s6 = 'learn'
s7 = 'failure'
quote1 = s2 + ' but as the ' + s4 + ' to ' + s6 + '.'
quote2 = 'It is fine to ' + s1 + ', ' + s3 + ' it is more important to ' + s5 + ' of ' + s7 + '.'
print()