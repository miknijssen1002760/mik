# DO I SAY HI?
# You just saw someone you know. You are not sure if you should say hi.
# This program will help you decide.

rememberName=True
isItYourEx=True
areYouDrunk=True
isItaFriendsEx=True
doYouWantToRekindle=False
areYouInAConvertible=False
areYouInABathRobe=True
canYouFlee=True
canYouPretendPhoneCall=True
areYouWearingSunglasses=True

if rememberName:
  if isItYourEx:
    if areYouDrunk:
      if doYouWantToRekindle:
        decision='Say hi.'
      else:
        decision='do NOT say hi'
    else:
      if areYouInAConvertible:
        decision='say hi!'
      else:
        'Do not say hi.'
  else:
    if isItaFriendsEx:
      decision='do not say hi'
    else:
      if areYouInABathRobe:
        decision='do not say hi!!!'
      else:
        decision='say hi :)'
else:
  if canYouFlee:
    decision='Run For It!'
  else:
      if canYouPretendPhoneCall:
        decision='Hello doctor. What are my text results?'
      else:
        if areYouWearingSunglasses:
          decision='Keep walking.'
        else:
          decision='Address the person using an amusing nickname such as Master Blaster.'
print()