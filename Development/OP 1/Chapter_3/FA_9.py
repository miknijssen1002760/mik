# You just saw someone you know, but you don't know the person's name.
# You are unsure of what to do. This program will help you decide.
canYouFlee=False
canYouPretendPhoneCall=True
areYouWearingSunglasses=True

if canYouFlee:
  decision='Run For It!'
else:
    if canYouPretendPhoneCall:
      decision='Hello doctor. What are my test results?'
    else:
      if areYouWearingSunglasses:
        decision='Keep walking.'
      else:
        decision='Address the person using an amusing nickname such as Master Blaster.'
print()