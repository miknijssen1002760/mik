points= 35
totalPoints = 50
print()
percentage = points/totalPoints*100
if percentage<50:
    remarks = 'Unsatisfactory'
elif percentage<70:
    remarks = 'Satisfactory'
elif percentage<90:
    remarks = 'Good'
else:
    remarks = 'Excellent'
print()