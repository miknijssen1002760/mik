number = 10
print()
if number == 0:
    status = '0 is even'
elif number%2 == 0:
    if number>0:
        status = str(number)+' is even and positive'
    else:
        status = str(number)+' is even and negative'
else:
    if number>0:
        status = str(number)+' is odd and positive'
    else:
        status = str(number)+' is odd and negative'
print()