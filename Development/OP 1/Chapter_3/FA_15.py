a = 123
b = 0

s = ''
if a > 10 and b < 100:
    s = s + 'a&b.'

if a < 100:
    s = s + 'a<100.'

if a > 0:
    s = s + 'a+ve.'
elif a < 0:
    s = s + 'a-ve.'
else:
    s = s + 'a0.'

if b > a:
    s = s + 'b++.'
elif a > b:
    s = s + 'a++.'
else:
    s = s + 'ab=.'

if a >= 500 or b >= 500:
    s = s + 'a|b>=500.'

if a - 101 != b + len('hello'):
    s = s + '?.'

if a % 10 == (a//100) + (a//10) % 10:
    s = s + '!.'

print()