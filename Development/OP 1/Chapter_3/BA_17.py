n = 123
print()

hundreds = n//100%10
tens = n//10%10
units = n%10
if n == (units*100+tens*10+hundreds):
    output = str(n)+' is a palindrome'
else:
    output = str(n)+' is NOT a palindrome'

print()