s = ''
x = 10
while x > 0:
  if x % 2 == 0:
    s = s + '*'
  elif x % 3 == 0:
    s = s + '#'
  else:
    s = s + '$'
  x = x - 1
print(s)