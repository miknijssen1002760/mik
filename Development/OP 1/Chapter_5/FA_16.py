outer = 1
stars = ''
while outer < 5:
  inner = outer
  while inner > 0:
    inner = inner - 1
    stars = stars + '*'
  outer = outer + 1
  stars = stars + '\n'
print()