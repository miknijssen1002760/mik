# Hint: this program creates the following shape:
# (1,1)(2,1)(3,1)
# (1,2)(2,2)(3,2)
# (1,3)(2,3)(3,3)
# (1,4)(2,4)(3,4)

# Which line would you change in this program
# in order to create a rectangle of 3 by 4 such
# as the one you see below?
# ***
# ***
# ***
# ***

# TIP: you could inspire yourself in
# the two previous FAs of this chapter

height = 4
width = 3
m = ''
row = 1
while row <= height:
  col = 1
  while col <= width:
    m = m + '(' + str(col) + ',' + str(row) + ')'
    col = col + 1
  m = m + '\n'
  row = row + 1
print(m)