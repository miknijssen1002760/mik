# Nested loops are useful to iterate through all cells of a matrix.
# For example, they are widely used to go through all the pixels of an image.
# Can you spot the difference between this and the previous FA assignment?

matrixHeight = 3
matrixWidth = 5
column = 1
whichColumn = ''
whichPoint = ''
while column <= matrixWidth:
  whichColumn = 'we are on column ' + str(column)
  row = 1
  while row <= matrixHeight:
    whichPoint = '(' + str(column) + ',' + str(row) + ')'
    row = row + 1
  column = column + 1
whichPoint = 'we left the matrix!'
whichColumn = whichPoint
print()