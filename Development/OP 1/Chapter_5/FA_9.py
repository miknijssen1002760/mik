# Nested loops are useful to iterate through all cells of a matrix.
# For example, they are widely used to go through all the pixels of an image

matrixHeight = 3
matrixWidth = 5
row = 1
whichRow = ''
whichPoint = ''
while row <= matrixHeight:
  whichRow = 'we are on row ' + str(row)
  column = 1
  while column <= matrixWidth:
    whichPoint = '(' + str(column) + ',' + str(row) + ')'
    column = column + 1
  row = row + 1
whichPoint = 'we left the matrix!'
whichRow = whichPoint
print()