h = 5
s = ''
height = h
while height>0:
  space=0
  width=0
  while space<height-1:
    s += ' '
    space += 1
  while width<(h-height+1):
    s += '*'
    width += 1
  s += '\n'
  height -= 1
print()