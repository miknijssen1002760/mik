x = 1
evenOrOdd = ''

while x < 6:
  if x % 2 == 0:
    evenOrOdd = str(x) + ' is even'
  else:
    evenOrOdd = str(x) + ' is odd'
  x = x + 1
print()