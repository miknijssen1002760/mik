x = 8
s = ''
while x > 0:
  if x % 3 == 0:
    s = s + '#'
  elif x % 2 == 0:
    s = s + '$'
  else:
    s = s + '*'
  x = x - 1
print()