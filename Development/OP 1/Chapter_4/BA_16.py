s = ''
start = 1
end = 30
print()

i=start
while i<=end:
    if i % 3 == 0 and i % 5 == 0:
        s += 'FizzBuzz'
    elif i % 3 == 0:
        s += 'Fizz'
    elif i % 5 == 0:
        s += 'Buzz'
    else:
        s += str(i)
    s+=' '
    i+=1

print()