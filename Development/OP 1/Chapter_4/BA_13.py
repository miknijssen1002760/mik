isItDay = False
isItNight = not isItDay

hour = 0
while hour<24:
  if 5<hour<18:
    isItDay = True
  else:
    isItDay = False
  isItNight = not isItDay
  hour = hour + 1

print()