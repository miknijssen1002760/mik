balance = 45000
interest = 5
years = 20

taxes_paid = 0
tax_50k = 1.5
tax_100k = 3

i = 0

while i < years:
    balance *= (1 + interest / 100)
    if balance > 50000 and balance <= 100000:
        tax = (balance - 50000) * (tax_50k / 100)
        taxes_paid += tax; balance -= tax
    elif balance > 100000:
        tax = (balance - 100000) * (tax_100k / 100) + (50000 * ( tax_50k / 100))
        taxes_paid += tax; balance -= tax
    i += 1

balance = round(balance, 2)
taxes_paid = round(taxes_paid, 2)
print()