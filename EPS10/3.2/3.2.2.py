import numpy as np
import matplotlib . pyplot as plt

x = np.linspace(0,4 * np.pi, 200)
y = np.sin(x)
z = np.cos(x)

plt.plot(x, y, 'r', label='y = sin(x)')
plt.plot(x, z, 'g', label='y = cos(x)')

plt.title('goniometrische functies')
plt.xlabel('x in rad', loc='right')
plt.ylabel('y', loc='top')

plt.grid()

plt.legend(loc=1)

plt.show()