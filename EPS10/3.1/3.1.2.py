import math
def mu(test_list):
    N=len(test_list)
    som=0
    for i in range(N):
        xi=test_list[i]
        som+=xi
    return (1/N)*som
    
def std_dev(test_list):
    N=len(test_list)
    som=0
    for i in range(N):
        xi=test_list[i]
        som+=(xi-mu(test_list))**2
    return math.sqrt((1/N))

test_list = [3.2, 5.7, 8.5, 9.1]
print(std_dev(test_list))
