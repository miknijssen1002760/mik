import statistics

def avg(cijfers):
    som = cijfers[0]
    for i in range(1, len(cijfers)):
        som = cijfers[i] + som
    return som/len(cijfers)

def test_avg(test_list):
    if avg(test_list) == statistics.mean(test_list):
        return'Gelijk'
    else:
        return'Niet gelijk'

test_list = [3.2, 5.7, 8.5, 9.1]
print(test_avg(test_list))