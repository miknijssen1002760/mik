def ggd(a, b):
    if a == b:
        r = a
    else:
        if a > b:
            r = ggd(a - b, b)
        else:
            r = ggd(a, b - a)
    
    return r

# Main
