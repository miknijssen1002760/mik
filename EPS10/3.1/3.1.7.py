def rendement_toets(resultaten):
    rendement = 0
    for i in range(0, len(resultaten)):
        if resultaten[i] >= 5.5:
            rendement += 1
    return rendement/len(resultaten)*100
    
toetsresultaten = [1.2 , 3.6 , 9.2 , 8.2 , 3.5 , 5.5 , 6.0 , 7.4 , 5.9 , 6.0 , 5.4 , 5.2 , 4.3 , 7.4 , 1.2 , 1.0]
print(rendement_toets(toetsresultaten), '%')
