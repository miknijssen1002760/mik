def ggd(a,b):
    if type(a) == str or type(a) == float:
        return "Error"
    if type(b) == str or type(b) == float:
        return "Error"
    if a < 0:
        a = a*-1
    if b < 0:
        b = b*-1
    if a < b:
        ggd(a,b - a)
        return ggd(a,b - a)
    elif a > b:
        ggd(a - b,b)
        return ggd(a - b,b)
    elif a == b:
        return a
    
def test_ggd():
    print(75, 105, ggd(175,105))
    print(15, -17.5, ggd(15,-17.5))
    print(0, 0, ggd("i", 0))
    print(1, 1000, ggd(1, 1000))

test_ggd()