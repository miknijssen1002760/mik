def ggd(a,b):
    if type(a) == str or type(a) == float:
        return'Error'
    if type(b) == str or type(b) == float:
        return'Error'
    if a < 0:
        a = a*-1
    if b < 0:
        b = b*-1
    while a != b:
        if a > b:
            a = a - b
        else:
            b = b - a
    return a

def test_ggd():
    print(175, 105, ggd(175, 105))
    print(15, 17.5, ggd(15, 17.5))
    print(15, -18, ggd(15, -18))
    print(0, 0, ggd('0', 0))
    print(1, 1000, ggd(1, 1000))
test_ggd()