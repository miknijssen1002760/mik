def factorial(n):
    res = 1
    for i in range(2, n+1):
        res = res*i
    return res
    
def test_factorial():
    print(factorial(1000), 1000)

test_factorial()