def fac(n):
    if n == 0:
        return 1
    else:
        recurse = fac(n-1)
        result = n * recurse
        return result
        
print(fac(1000))