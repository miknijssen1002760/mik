def input_int(prompt, min, max):
    n = int(input(prompt+'\n'))
    if min <= n <= max:
        return n
    else:
        return input_int(prompt, min, max)
    
def test_input_int():
    r1 = input_int('wat is je eerste cijfer?', 1, 10)
    r2 = input_int('wat is je tweede cijfer?', 1, 10)
    return (r1 + r2)/2
    
print(test_input_int())