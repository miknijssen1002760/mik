i1 = input()
i2 = input()
i3 = input()

if i1 == ['PK', 'SR', 'GD', 'BK', 'BN', 'RD', 'OG', 'YE', 'GN', 'BU', 'VT', 'GY', 'WH']:
    i1 = ['-3', '-2', '-1', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9']

# 'SR' = '-2'
# 'GD' = '-1'
# 'BK' = '0'
# 'BN' = '1'
# 'RD' = '2'
# 'OG' = '3'
# 'YE' = '4'
# 'GN' = '5'
# 'BU' = '6'
# 'VT' = '7'
# 'GY' = '8'
# 'WH' = '9'

def calc_E12_base(n):
    if 1 <= n <= 12:
        return round(10 * 10**((n-1)/12))
    else:
        return -1
    
def E12_base(n):
    if 1 <= n <= 5 or 10 <= n <= 11:
        return calc_E12_base(n)
    elif 6 <= n <= 9:
        return calc_E12_base(n)+1
    elif n == 12:
        return calc_E12_base(n)-1
    else:
        return -1
    
def E12_value(n, p):
    if -3 <= p <=9:
        return E12_base(n)*10**(p-1)
    else:
        return -1

#def test_resistor():
    E12_value(int(i1+i2), i3)
print(i1)
