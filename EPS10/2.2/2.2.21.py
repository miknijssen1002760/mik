import math

def calc_E12_base(n):
    if 1 <= n <= 12:
        return round(10**((n-1)/12), 1)
    else:
        return -1

def E12_base(n):
    if 1 <= n <= 5 or 10 <= n <= 11:
        return calc_E12_base(n)
    elif 6 <= n <= 9:
        return calc_E12_base(n)+0.1
    elif n == 12:
        return calc_E12_base(n)-0.1
    else:
        return -1

def E12_value(n, p):
    if -3 <= p <=12:
        return E12_base(n)*10**(p)
    else:
        return -1

def print_E12_value():
    for p in range(-2, 11):
        for n in range(1, 13):
            if math.log(E12_value(n, p)) <  0:
                print(str(round(E12_value(n, p)*10**3,1))+" mΩ  ")
            elif math.log(E12_value(n, p)) <= 6.907755278982137:
                print(str(round(E12_value(n, p)/10,1))+" Ω  ")
            elif math.log(E12_value(n, p)) <= 13.815510557964274:
                print(str(round(E12_value(n, p)*10**-3,1))+" KΩ  ")
            elif math.log(E12_value(n, p)) <= 20.72326583694641:
                print(str(round(E12_value(n, p)*10**-6,1))+" MΩ  ")
            elif math.log(E12_value(n, p)) <= 27.631021115928547:
                print(str(round(E12_value(n, p)*10**-9,1))+" GΩ  ")
            else:
                print(-1)
    
def test_calc_E12_base():
    for i in range(0, 14):
        print(i, round(calc_E12_base(i),1))
    
def test_E12_base():
    for i in range(0, 14):
        print(i, round(E12_base(i),1))
    
def test_E12_value():
    n = int(input('geef een waarde voor n: '))
    p = int(input('geef een waarde voor p: '))
    print(E12_value(n, p))

#print_E12_value()
#test_E12_value()
#test_calc_E12_base()
#test_E12_base()
#print(calc_E12_base(9))