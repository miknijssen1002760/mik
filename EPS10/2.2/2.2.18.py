def factorial(n):
    i = 1
    res = 1
    while n+1 > i:
        res = res * i
        i = i + 1
    return res
    
def test_factorial():
    print(factorial(3))

test_factorial()