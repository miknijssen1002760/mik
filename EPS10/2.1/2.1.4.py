import math

def print_aantal_wortels(a, b, c):
    if(b**2-4*a*c == 0):
        print('Deze vierkantsvergelijking heeft één reële wortel.')
    elif(b**2-4*a*c > 0):
        print('Deze vierkantsvergelijking heeft twee reële wortels.')
    else:
        print('Deze vierkantsvergelijking heeft geen reële wortels.')

def print_wortels(a, b, c):
    x1 = (-b + math.sqrt(b**2-4*a*c))/(2*a)
    x2 = (-b - math.sqrt(b**2-4*a*c))/(2*a)
    if(b**2-4*a*c == 0):
        print('Deze vierkantsvergelijking heeft één reële wortel.')
        print(x1)
    if(b**2-4*a*c > 0):
        print('Deze vierkantsvergelijking heeft twee reële wortels.')
        print(x1)
        print(x2)
    else:
        print('Deze vierkantsvergelijking heeft geen reële wortels.')