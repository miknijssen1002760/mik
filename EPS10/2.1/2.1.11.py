def print_iets(n):
    lsb = n % 2
    if n > 0:
        print_iets(n // 2)
        print(lsb, end='')
    else:
        print()

print_iets(2**3000)
