def klok(tijd):
    minuten_in_uur = tijd//60
    seconden = tijd%60
    uren = minuten_in_uur//60
    minuten = minuten_in_uur%60
    print(uren, ' uur ', minuten, ' minuten en ', seconden, ' seconden')
